package com.formation.epsi.java.superherogestion.service;

import com.formation.epsi.java.superherogestion.dto.SuperHeroDto;
import com.formation.epsi.java.superherogestion.model.SuperHeroEntity;

import java.util.List;

public interface SuperHeroService {

    SuperHeroDto get(Long id) throws Exception;

    List<SuperHeroDto> findAll();

    SuperHeroEntity add(SuperHeroDto superHeroDto);

    SuperHeroEntity update(Long id, SuperHeroDto superHeroDto) throws Exception;

    void delete(Long id);
}
