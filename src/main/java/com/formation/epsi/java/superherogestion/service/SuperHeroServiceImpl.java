package com.formation.epsi.java.superherogestion.service;

import com.formation.epsi.java.superherogestion.dto.SuperHeroDto;
import com.formation.epsi.java.superherogestion.mapper.SuperHeroMapper;
import com.formation.epsi.java.superherogestion.model.SuperHeroEntity;
import com.formation.epsi.java.superherogestion.repository.SuperHeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SuperHeroServiceImpl implements SuperHeroService {

    @Autowired
    private SuperHeroRepository superHeroRepository;

    @Autowired
    private SuperHeroMapper superHeroMapper;

    @Override
    public SuperHeroDto get(Long id) throws Exception {
        return this.checkHeroAndReturn(id);
    }

    @Override
    public List<SuperHeroDto> findAll() {
        List<SuperHeroEntity> superHeroEntities = superHeroRepository.findAll();
        return superHeroMapper.entitiesToSuperHeroDtos(superHeroEntities);
    }

    @Override
    public SuperHeroEntity add(SuperHeroDto superHeroDto) {
        return superHeroRepository.save(superHeroMapper.dtoToSuperHeroEntity(superHeroDto));
    }

    @Override
    public SuperHeroEntity update(Long id, SuperHeroDto superHeroDto) throws Exception {
        superHeroDto.setId(id);
        return superHeroRepository.save(superHeroMapper.dtoToSuperHeroEntity(superHeroDto));
    }

    @Override
    public void delete(Long id) {
        superHeroRepository.delete(superHeroRepository.getById(id));
    }

    private SuperHeroDto checkHeroAndReturn(Long id) throws Exception {
        Optional<SuperHeroEntity> optSuperHeroEntity = superHeroRepository.findById(id);
        if (optSuperHeroEntity.isPresent()) {
            return superHeroMapper.entityToSuperHeroDto(optSuperHeroEntity.get());
        } else throw new Exception();
    }
}
