package com.formation.epsi.java.superherogestion.controller;

import com.formation.epsi.java.superherogestion.dto.SuperHeroDto;
import com.formation.epsi.java.superherogestion.model.SuperHeroEntity;
import com.formation.epsi.java.superherogestion.service.SuperHeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/v1/superheroes")
public class SuperHeroController {

    @Autowired
    private SuperHeroService superHeroService;

    @GetMapping("/{id}")
    public ResponseEntity<SuperHeroDto> get(@PathVariable("id") Long id) throws Exception {
        return new ResponseEntity<>(superHeroService.get(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<SuperHeroDto>> getAll() {
        return new ResponseEntity<>(superHeroService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<SuperHeroEntity> add(@RequestBody SuperHeroDto superHeroDto) {
        return new ResponseEntity<>(superHeroService.add(superHeroDto), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<SuperHeroEntity> update(@PathVariable("id") Long id, @RequestBody SuperHeroDto superHeroDto) throws Exception {
        return new ResponseEntity<>(superHeroService.update(id, superHeroDto), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        superHeroService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
