package com.formation.epsi.java.superherogestion;

import com.formation.epsi.java.superherogestion.model.PowerEntity;
import com.formation.epsi.java.superherogestion.model.SuperHeroEntity;
import com.formation.epsi.java.superherogestion.repository.PowerRepository;
import com.formation.epsi.java.superherogestion.repository.SuperHeroRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
@RequiredArgsConstructor
public class SuperherogestionApplication {

    @Autowired
    private final PowerRepository powerRepository;

    @Autowired
    private final SuperHeroRepository superHeroRepository;

    public static void main(String[] args) {
        SpringApplication.run(SuperherogestionApplication.class, args);
    }

    @PostConstruct
    public void initData() {

        PowerEntity power = PowerEntity.builder().name("Vol").description("Permet de voler").build();
        powerRepository.save(power);

        PowerEntity power1 = PowerEntity.builder().name("Force surhumaine").build();
        powerRepository.save(power1);

        Set<PowerEntity> powers = new HashSet<>(powerRepository.findAll());

        SuperHeroEntity superHero = SuperHeroEntity.builder().superHeroName("SuperMan").secretIdentity("Clark Kent").powers(powers).build();

        SuperHeroEntity createSuperHero = superHeroRepository.save(superHero);

        System.out.println(createSuperHero.getSuperHeroName() + " possède le(s) pouvoir(s) : ");
        createSuperHero.getPowers().forEach(p -> System.out.println("- " + p.getName()));
    }
}
