package com.formation.epsi.java.superherogestion.mapper;

import com.formation.epsi.java.superherogestion.dto.SuperHeroDto;
import com.formation.epsi.java.superherogestion.model.SuperHeroEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SuperHeroMapper {

    SuperHeroDto entityToSuperHeroDto(SuperHeroEntity entity);

    List<SuperHeroDto> entitiesToSuperHeroDtos(List<SuperHeroEntity> entities);

    SuperHeroEntity dtoToSuperHeroEntity(SuperHeroDto dto);
}
