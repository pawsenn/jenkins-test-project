package com.formation.epsi.java.superherogestion.service;

import com.formation.epsi.java.superherogestion.dto.PowerDto;
import com.formation.epsi.java.superherogestion.mapper.PowerMapper;
import com.formation.epsi.java.superherogestion.model.PowerEntity;
import com.formation.epsi.java.superherogestion.repository.PowerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PowerServiceImpl implements PowerService {

    @Autowired
    private PowerRepository powerRepository;

    @Autowired
    private PowerMapper powerMapper;

    @Override
    public PowerDto get(Long id) throws Exception {
        return this.checkHeroAndReturn(id);
    }

    @Override
    public List<PowerDto> findAll() {
        return null;
    }

    @Override
    public PowerEntity add(PowerDto powerDto) {
        return null;
    }

    @Override
    public PowerEntity update(Long id, PowerDto powerDto) throws Exception {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<PowerDto> getByName(String powerName) {
        return powerMapper.entitiesToPowerrHeroDtos(powerRepository.findByNameContains(powerName));
    }

    private PowerDto checkHeroAndReturn(Long id) throws Exception {
        Optional<PowerEntity> optSuperHeroEntity = powerRepository.findById(id);
        if (optSuperHeroEntity.isPresent()) {
            return powerMapper.entityToPowerDto(optSuperHeroEntity.get());
        } else throw new Exception();
    }
}
