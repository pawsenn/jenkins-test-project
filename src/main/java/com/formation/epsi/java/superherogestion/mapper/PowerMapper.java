package com.formation.epsi.java.superherogestion.mapper;

import com.formation.epsi.java.superherogestion.dto.PowerDto;
import com.formation.epsi.java.superherogestion.model.PowerEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PowerMapper {

    PowerDto entityToPowerDto(PowerEntity entity);

    List<PowerDto> entitiesToPowerrHeroDtos(List<PowerEntity> entities);

    PowerEntity dtoToPowerEntity(PowerDto dto);
}
