package com.formation.epsi.java.superherogestion.service;

import com.formation.epsi.java.superherogestion.dto.PowerDto;
import com.formation.epsi.java.superherogestion.model.PowerEntity;

import java.util.List;

public interface PowerService {

    PowerDto get(Long id) throws Exception;

    List<PowerDto> findAll();

    PowerEntity add(PowerDto powerDto);

    PowerEntity update(Long id, PowerDto powerDto) throws Exception;

    void delete(Long id);

    List<PowerDto> getByName(String powerName);
}
