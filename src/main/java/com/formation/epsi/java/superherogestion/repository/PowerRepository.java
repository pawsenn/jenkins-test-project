package com.formation.epsi.java.superherogestion.repository;

import com.formation.epsi.java.superherogestion.model.PowerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PowerRepository extends JpaRepository<PowerEntity, Long> {

//    @Query("select p from PowerEntity p where p.name like %?1%")
    List<PowerEntity> findByNameContains(String name);
}
