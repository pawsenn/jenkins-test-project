package com.formation.epsi.java.superherogestion.repository;

import com.formation.epsi.java.superherogestion.model.VillainEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VillainRepository extends JpaRepository<VillainEntity, Long> {
}
