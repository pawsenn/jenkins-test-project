package com.formation.epsi.java.superherogestion.controller;

import com.formation.epsi.java.superherogestion.dto.PowerDto;
import com.formation.epsi.java.superherogestion.service.PowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(value = "/v1/powers")
public class PowerController {

    @Autowired
    PowerService powerService;

    @GetMapping("/search")
    public ResponseEntity<List<PowerDto>> search(@RequestParam("powerName") String powerName) throws Exception {
        return new ResponseEntity<>(powerService.getByName(powerName), HttpStatus.OK);
    }

}
