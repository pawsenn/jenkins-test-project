package com.formation.epsi.java.superherogestion.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "villains")
public class VillainEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "villains")
    private Set<SuperHeroEntity> superHeroes;

    @OneToOne(mappedBy = "nemesis")
    private SuperHeroEntity superHero;
}
