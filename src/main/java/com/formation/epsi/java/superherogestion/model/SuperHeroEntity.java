package com.formation.epsi.java.superherogestion.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "super_heroes")
public class SuperHeroEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "super_hero_name", nullable = false)
    private String superHeroName;

    @Column(name = "secret_identity", nullable = false)
    private String secretIdentity;

    @ManyToMany
    @JoinTable(
            name = "super_hero_has_power",
            joinColumns = @JoinColumn(name = "super_hero_id"),
            inverseJoinColumns = @JoinColumn(name = "power_id")
    )
    private Set<PowerEntity> powers;

    @ManyToMany
    @JoinTable(
            name = "super_hero_fights_villain",
            joinColumns = @JoinColumn(name = "super_hero_id"),
            inverseJoinColumns = @JoinColumn(name = "villain_id")
    )
    private Set<VillainEntity> villains;

    @OneToOne
    @JoinColumn(name = "nemesis_id", referencedColumnName = "id")
    private VillainEntity nemesis;

    @ManyToOne
    @JoinColumn(name = "mentor_id")
    private SuperHeroEntity mentor;

    @OneToMany(mappedBy = "mentor")
    private Set<SuperHeroEntity> sidekicks;
}
