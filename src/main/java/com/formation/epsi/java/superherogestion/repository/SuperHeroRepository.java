package com.formation.epsi.java.superherogestion.repository;

import com.formation.epsi.java.superherogestion.model.SuperHeroEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperHeroRepository extends JpaRepository<SuperHeroEntity, Long> {
}
